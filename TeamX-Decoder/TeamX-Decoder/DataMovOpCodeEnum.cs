﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TeamX_Decoder
{
	enum DataMovOpCodeEnum
	{
		mov = 0b00,
		str = 0b01,
		ld = 0b10,
		invalid = -1

	}
}
