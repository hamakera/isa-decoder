﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TeamX_Decoder
{
	enum RegisterEnum
	{
		AC =	0b000,
		ACC =	0b001,
		SP =	0b011,
		CNT =	0b100,
		BS =	0b110,
		INVALID = -1

	}
}
