﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TeamX_Decoder
{
	enum InstructionTypeEnum
	{
		AL=				0b0000000000000000,
		DataMovement=	0b0100000000000000,
		Conditional=	0b1000000000000000

	}
}
