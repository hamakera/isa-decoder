﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TeamX_Decoder
{
	enum FlagNumEnum
	{
		NONE =	0b000,
		EQ =	0b001,
		Z =		0b010,
		OF =	0b011,
		C =		0b100,
		GT =	0b101,
		LT =	0b110,
		INVALID = -1
	}
}
