﻿/******************************************************************
 * Team Name: Team X
 * Programmers: Alec Hamaker
 * Date Created: 3/9/2021
 * Date Last Modified: 3/9/2021
 * FileName: DataMovementDecoder.cs
 * Purpose: This class will assist in the decoding of all the of 
 * the data movement instructions
 ******************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace TeamX_Decoder
{
	class DataMovementDecoder
	{
		private static int InstructionTypeMask =	0b1100000000000000;
		private static int DestRegMask =			0b0000111000000000;
		private static int SrcRegMask =				0b0000000011100000;
		private static int OffsetMask =				0b0000000000011111;
		private static int ImmediateOffset =		0b0000000011111111;
		private static int OffsetInstructionMask =		0b0000000100000000;
		public static RegisterEnum GetDestinationRegister(int instruction)
		{
			if ((InstructionTypeEnum)(InstructionTypeMask & instruction) == InstructionTypeEnum.DataMovement)
				return (RegisterEnum)((DestRegMask & instruction) >> 9);
			else
				return RegisterEnum.INVALID;
		}

		public static RegisterEnum GetSourceRegister(int instruction)
		{ 
			if ((InstructionTypeEnum)(InstructionTypeMask & instruction) == InstructionTypeEnum.DataMovement)
				return (RegisterEnum)((SrcRegMask & instruction) >> 5);
			else
				return RegisterEnum.INVALID;
		}

		public static int GetOffset(int instruction)
		{
			if ((InstructionTypeEnum)(InstructionTypeMask & instruction) == InstructionTypeEnum.DataMovement &&
				((OffsetInstructionMask & instruction) >> 8) == 1)
				return (OffsetMask & instruction);
			else
				return -1;
		}
		public static int GetImmediate(int instruction)
		{
			if ((InstructionTypeEnum)(InstructionTypeMask & instruction) == InstructionTypeEnum.DataMovement &&
				((OffsetInstructionMask & instruction) >> 8) == 0)
				return (ImmediateOffset & instruction);
			else
				return -1;
		}

	}
}
