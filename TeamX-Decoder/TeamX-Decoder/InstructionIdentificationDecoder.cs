﻿/******************************************************************
 * Team Name: Team X
 * Programmers: Alec Hamaker
 * Date Created: 3/9/2021
 * Date Last Modified: 3/9/2021
 * FileName: InstructionIdentificationDecoder.cs
 * Purpose: This class will assist in the early steps of decoding
 * and identifying an instruction
 ******************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace TeamX_Decoder
{
	class InstructionIdentificationDecoder
	{
		private static int InstructionTypeMask =	0b1100000000000000;
		private static int ALOpCodeMask =			0b0011100000000000;
		private static int DataMovOpCodeMask =		0b0011000000000000;
		private static int ConditionalOpCodeMask =	0b0010000000000000;


		public static InstructionTypeEnum GetInstructionType(int instruction)
		{
			return (InstructionTypeEnum)(InstructionTypeMask & instruction);
		}

		public static ALOpCodeEnum GetALOpCode(int instruction)
		{
			//verify that the instruction is a AL instruction
			if ((InstructionTypeMask & instruction) == 0b0)
				return (ALOpCodeEnum)((ALOpCodeMask & instruction) >> 11);
			else
				return ALOpCodeEnum.INVALID;
		}

		public static DataMovOpCodeEnum GetDataMovOpCode(int instruction)
		{
			//verify that the instruction is a data mov instruction
			if ((InstructionTypeMask & instruction) == 0b0100000000000000)
				return (DataMovOpCodeEnum)((DataMovOpCodeMask & instruction) >> 12);
			else
				return DataMovOpCodeEnum.invalid;
		}

		public static ConditionalOpCodeEnum GetConditionalOpCode(int instruction)
		{
			//verify that the instruction is a conditional instruction
			if ((InstructionTypeMask & instruction) == 0b1000000000000000)
				return (ConditionalOpCodeEnum)((ConditionalOpCodeMask & instruction) >> 13);
			else
				return ConditionalOpCodeEnum.invalid;
		}
	}
}
