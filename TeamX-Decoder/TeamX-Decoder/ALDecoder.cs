﻿/******************************************************************
 * Team Name: Team X
 * Programmers: Alec Hamaker
 * Date Created: 3/9/2021
 * Date Last Modified: 3/9/2021
 * FileName: ALDecoder.cs
 * Purpose: This class will assist in the decoding of all the of 
 * the arithmetic and logic instructions
 ******************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace TeamX_Decoder
{
	class ALDecoder
	{
		private static int DestRegMask =	0b0000011100000000;
		private static int SrcRegMask =		0b0000000011100000;
		private static int FlagNumMask =	0b0000000000011100;
		private static int InstructionTypeMask =	0b1100000000000000;
		public static RegisterEnum GetDestinationRegister(int instruction)
		{
			if ((InstructionTypeEnum)(InstructionTypeMask & instruction) == InstructionTypeEnum.AL)
				return (RegisterEnum)((instruction & DestRegMask) >> 8);
			else
				return RegisterEnum.INVALID;
		}

		public static RegisterEnum GetSrcRegister(int instruction)
		{ 
			if ((InstructionTypeEnum)(InstructionTypeMask & instruction) == InstructionTypeEnum.AL)
				return (RegisterEnum)((instruction & SrcRegMask) >> 5);
			else
				return RegisterEnum.INVALID;
		}

		public static FlagNumEnum GetFlagNum(int instruction)
		{

			if ((InstructionTypeEnum)(InstructionTypeMask & instruction) == InstructionTypeEnum.AL)
				return (FlagNumEnum)((instruction & FlagNumMask) >> 2);
			else
				return FlagNumEnum.INVALID;
		}

	}
}
