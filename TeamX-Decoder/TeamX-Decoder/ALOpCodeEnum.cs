﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TeamX_Decoder
{
	enum ALOpCodeEnum
	{
		ADD = 0b000,
		SUB = 0b001,
		LSH = 0b010,
		RSH = 0b011,
		AND = 0b100,
		OR = 0b101,
		NOT = 0b110,
		INVALID = -1

	}
}
