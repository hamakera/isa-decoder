﻿using System;
using UtilityNamespace;

namespace TeamX_Decoder
{
	class Program
	{
		static int instruction = -1;
		static InstructionTypeEnum type;
		static RegisterEnum DestReg;
		static RegisterEnum SrcReg;
		static FlagNumEnum FlagNum;
		static int Offset;
		static int Immediate;
		static string mnemonic = "";
		static bool done = false;

		[STAThread]
		static void Main(string[] args)
		{
			Menu mainMenu = new Menu();
			mainMenu = mainMenu + "Enter Hex instruction" + "Enter Decimal instruction" + "Enter Binary instruction" + "Quit";

			Choices choice = (Choices)mainMenu.GetChoice();
			while (choice != Choices.QUIT)
			{
				switch (choice)
				{
					case Choices.HEX:
						HandleHex();
						break;
					case Choices.BINARY:
						HandleBinary();
						break;
					case Choices.DECIMAL:
						HandleDecimal();
						break;
				}
				choice = (Choices)mainMenu.GetChoice();
			}
		}

		private static void HandleBinary()
		{
			Console.ForegroundColor = ConsoleColor.Gray;
			bool validInput = false;
			while (!validInput)
			{
				Console.Clear();
				Console.Write("Please enter a binary instruction: ");
				string input = Console.ReadLine();
				try
				{
					int tmp = Convert.ToInt32(input, 2);
					validInput = true;
					instruction = tmp;
				}
				catch (Exception e)
				{
					Console.WriteLine("Invalid input..");
					validInput = false;
					Console.WriteLine("Press any key to continue. . .");
					Console.ReadKey();
				}
			}
			DoAnalysis();
		}

		private static void HandleHex()
		{
			Console.ForegroundColor = ConsoleColor.Gray;
			bool validInput = false;
			while (!validInput)
			{
				Console.Clear();
				Console.Write("Please enter a hexadecimal instruction: ");
				string input = Console.ReadLine();
				try
				{
					int tmp = Convert.ToInt32(input, 16);
					validInput = true;
					instruction = tmp;
				}
				catch (Exception e)
				{
					Console.WriteLine("Invalid input..");
					validInput = false;
					Console.WriteLine("Press any key to continue. . .");
					Console.ReadKey();
				}
			}
			DoAnalysis();
		}

		private static void HandleDecimal()
		{
			Console.ForegroundColor = ConsoleColor.Gray;
			bool validInput = false;
			while (!validInput)
			{
				Console.Clear();
				Console.Write("Please enter a decimal instruction: ");
				string input = Console.ReadLine();
				validInput = int.TryParse(input, out int tmp);
				if (validInput)
					instruction = tmp;
			}
			DoAnalysis();
		}

		private static void DoAnalysis()
		{
			Console.ForegroundColor = ConsoleColor.Gray;
			type = InstructionIdentificationDecoder.GetInstructionType(instruction);
			switch (type)
			{
				case InstructionTypeEnum.AL:
					mnemonic = InstructionIdentificationDecoder.GetALOpCode(instruction).ToString();
					DestReg = ALDecoder.GetDestinationRegister(instruction);
					SrcReg = ALDecoder.GetSrcRegister(instruction);
					FlagNum = ALDecoder.GetFlagNum(instruction);
					PrintALAnalysis();
					break;
				case InstructionTypeEnum.Conditional:
					mnemonic = InstructionIdentificationDecoder.GetConditionalOpCode(instruction).ToString();
					DestReg = ConditionalDecoder.GetRegister1(instruction);
					SrcReg = ConditionalDecoder.GetRegister2(instruction);
					FlagNum = ConditionalDecoder.GetFlagNum(instruction);
					PrintConditionalAnalysis();
					break;
				case InstructionTypeEnum.DataMovement:
					mnemonic = InstructionIdentificationDecoder.GetDataMovOpCode(instruction).ToString();
					DestReg = DataMovementDecoder.GetDestinationRegister(instruction);
					SrcReg = DataMovementDecoder.GetSourceRegister(instruction);
					Offset = DataMovementDecoder.GetOffset(instruction);
					Immediate = DataMovementDecoder.GetImmediate(instruction);
					PrintDataMovementAnalysis();
					break;
			}



		}

		private static void PrintDataMovementAnalysis()
		{
			Console.Clear();
			Console.WriteLine($"Instruction Type:\t\t\t{type}");
			Console.WriteLine($"Instruction Mnemonic:\t\t\t{mnemonic}");
			Console.WriteLine($"Instruction Destination Register:\t{DestReg}");
			Console.WriteLine($"Instruction Source Register:\t\t{SrcReg}");
			if (Offset == -1)
				Console.WriteLine("Offset:\t\t\t\tN/A");
			else
				Console.WriteLine($"Offset:\t\t\t\t\t{Offset}");
			if (Immediate == -1)
				Console.WriteLine("Immediate:\t\t\t\tN/A");
			else
				Console.WriteLine($"Immediate:\t\t\t\t{Immediate}");
			Console.WriteLine();
			Console.WriteLine();
			DataMovOpCodeEnum opCode = InstructionIdentificationDecoder.GetDataMovOpCode(instruction);
			string srcRegName = DetermineRegisterName(SrcReg);
			string destRegName = DetermineRegisterName(DestReg);
			switch (opCode)
			{
				case DataMovOpCodeEnum.ld:
					Console.WriteLine($"The instruction was to load the value from the {srcRegName} register with the offset of {Offset} into the {destRegName} register.");
					break;
				case DataMovOpCodeEnum.mov:
					Console.WriteLine($"The instruction was to move the immediate value {Immediate} into the {destRegName} register.");
					break;
				case DataMovOpCodeEnum.str:
					Console.WriteLine($"The instuction was to store the value from the {destRegName} register to the address stored in the {srcRegName} register with an offset of {Offset}.");
					break;
			}
			PrintFlagInfo(FlagNum);
			Console.WriteLine("Press any key to continue. . .");
			Console.ReadKey();
		}
		private static void PrintConditionalAnalysis()
		{
			Console.Clear();
			Console.WriteLine($"Instruction Type:\t\t\t{type}");
			Console.WriteLine($"Instruction Mnemonic:\t\t\t{mnemonic}");
			Console.WriteLine($"Instruction Register 1:\t\t\t{DestReg}");
			Console.WriteLine($"Instruction Register 2:\t\t\t{SrcReg}");
			Console.WriteLine($"Flag Number:\t\t\t\t{FlagNum}");
			Console.WriteLine();
			Console.WriteLine();
			ConditionalOpCodeEnum opCode = InstructionIdentificationDecoder.GetConditionalOpCode(instruction);
			string srcRegName = DetermineRegisterName(SrcReg);
			string destRegName = DetermineRegisterName(DestReg);
			if (opCode == ConditionalOpCodeEnum.br)
				Console.WriteLine($"The instruction was to branch to the address stored in the {destRegName} register with the offset stored in the {srcRegName} register.");
			else
				Console.WriteLine($"The instruction was to compare the value stored in the {destRegName} register to the value stored in the {srcRegName} register and set the corresponding flags,");
			PrintFlagInfo(FlagNum);
			Console.WriteLine("Press any key to continue. . .");
			Console.ReadKey();
		}

		private static void PrintALAnalysis()
		{
			Console.Clear();
			Console.WriteLine($"Instruction Type:\t\t\t{type}");
			Console.WriteLine($"Instruction Mnemonic:\t\t\t{mnemonic}");
			Console.WriteLine($"Instruction Destination Register:\t{DestReg}");
			Console.WriteLine($"Instruction Source Register:\t\t{SrcReg}");
			Console.WriteLine($"Flag Number:\t\t\t\t{FlagNum}");
			Console.WriteLine();
			Console.WriteLine();
			ALOpCodeEnum opCode = InstructionIdentificationDecoder.GetALOpCode(instruction);
			string srcRegName = DetermineRegisterName(SrcReg);
			string destRegName = DetermineRegisterName(DestReg);
			if (opCode == ALOpCodeEnum.LSH || opCode == ALOpCodeEnum.RSH)
				Console.WriteLine($"The instruction was to {(opCode == ALOpCodeEnum.LSH ? "left shift" : "right shift")} the value in the {srcRegName} register by the value in the {destRegName} register and store the result in the {destRegName} register.");
			else
				Console.WriteLine($"The instruction was to {mnemonic.ToLower()} the value in the {srcRegName} register to the value in the {destRegName} register and store the result into the {destRegName} register.");
			PrintFlagInfo(FlagNum);
			Console.WriteLine("Press any key to continue. . .");
			Console.ReadKey();
		}

		private static string DetermineFlagName(FlagNumEnum flagNum)
		{
			string flagName = "";
			switch (FlagNum)
			{
				case FlagNumEnum.C:
					flagName = "carry";
					break;
				case FlagNumEnum.EQ:
					flagName = "equal";
					break;
				case FlagNumEnum.GT:
					flagName = "greater than";
					break;
				case FlagNumEnum.LT:
					flagName = "less than";
					break;
				case FlagNumEnum.OF:
					flagName = "over flow";
					break;
				case FlagNumEnum.Z:
					flagName = "zero";
					break;
			}
			return flagName;
		}

		private static string DetermineRegisterName(RegisterEnum register)
		{
			string registerName = "";
			switch (register)
			{
				case RegisterEnum.AC:
					registerName = "first accumulator";
					break;
				case RegisterEnum.ACC:
					registerName = "second accumulator";
					break;
				case RegisterEnum.BS:
					registerName = "base";
					break;
				case RegisterEnum.CNT:
					registerName = "count";
					break;
				case RegisterEnum.SP:
					registerName = "stack pointer";
					break;
			}
			return registerName;
		}

		private static void PrintFlagInfo(FlagNumEnum flagNum)
		{ 
			if (FlagNum != FlagNumEnum.NONE)
				Console.WriteLine($"This instruction specified to only execute if the {DetermineFlagName(FlagNum)} flag was set in the flags register.");
		}
	}
}
