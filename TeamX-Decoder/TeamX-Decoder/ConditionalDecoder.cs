﻿/******************************************************************
 * Team Name: Team X
 * Programmers: Alec Hamaker
 * Date Created: 3/9/2021
 * Date Last Modified: 3/9/2021
 * FileName: ConditionalDecoder.cs
 * Purpose: This class will assist in the decoding of all the of 
 * the conditional instructions 
 ******************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace TeamX_Decoder
{
	class ConditionalDecoder
	{
		private static int InstructionTypeMask =	0b1100000000000000;
		private static int Register1Mask =			0b0001110000000000;
		private static int Register2Mask =			0b0000001110000000;
		private static int FlagNumMask =			0b0000000001110000;

		public static RegisterEnum GetRegister1(int instruction)
		{
			if ((InstructionTypeEnum)(InstructionTypeMask & instruction) == InstructionTypeEnum.Conditional)
				return (RegisterEnum)((Register1Mask & instruction) >> 10);
			else
				return RegisterEnum.INVALID;
		}
		public static RegisterEnum GetRegister2(int instruction)
		{
			if ((InstructionTypeEnum)(InstructionTypeMask & instruction) == InstructionTypeEnum.Conditional)
				return (RegisterEnum)((Register2Mask & instruction) >> 7);
			else
				return RegisterEnum.INVALID;
		}
		public static FlagNumEnum GetFlagNum(int instruction)
		{
			if ((InstructionTypeEnum)(InstructionTypeMask & instruction) == InstructionTypeEnum.Conditional)
				return (FlagNumEnum)((FlagNumMask & instruction) >> 4);
			else
				return FlagNumEnum.INVALID;
		}
	}
}
